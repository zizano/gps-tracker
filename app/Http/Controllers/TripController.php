<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Trip;

class TripController extends Controller
{
    
    public function __construct() {

        $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'destroy', 'update']]);

    }    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trips = Auth::user()->trips;

        return view('trips.index', compact('trips'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validation = $this->validator($request->all());

        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        $trip_name = $request->get('name');
        $file = $request->file('trip');
        $file_name = md5(uniqid()) . '.gpx';
        
        $file_path = public_path('files/gmaps/') . Auth::user()->id;
        
        if (!file_exists($file_path)) {
            File::makeDirectory($file_path, 0777, true);
        }

        $file->move($file_path, $file_name);

        $trip = new Trip([
            'name' => $trip_name,
            'file' => Auth::user()->id . '/' . $file_name
        ]);

        Auth::user()->trips()->save($trip);

        return response()->redirectTo('/trips')
            ->with('message', "{$trip_name} created");
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'trip' => 'required',
        ]);
    }
    
}
