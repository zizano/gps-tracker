@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create trip</div>

                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/trips" method="POST" enctype="multipart/form-data" files="true">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Trip Name</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Please add trip name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label for="file">Trip File:</label>
                                <input type="file" id="file" name="trip">
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection